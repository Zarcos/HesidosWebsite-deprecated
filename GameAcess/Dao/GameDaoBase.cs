﻿using System.Collections.Generic;
using GameAcess.Interface;
using NHibernate;

namespace GameAcess.Dao
{
    public class GameDaoBase<T> : IGameDaoBase<T> where T : class
    {
        protected ISession Session;

        protected GameDaoBase()
        {
            Session = NHibernateSessionGame.Session;
        }

        public IList<T> GetAll()
        {
            return Session.QueryOver<T>().List<T>();
        }

        public void Update(T entity)
        {
            using (var transaction = Session.BeginTransaction())
            {
                Session.Update(entity);
                transaction.Commit();
            }
        }
    }
}