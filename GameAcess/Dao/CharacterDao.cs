﻿using System.Collections.Generic;
using GameAcess.Model;
using NHibernate.Criterion;

namespace GameAcess.Dao
{
    public class CharacterDao : GameDaoBase<Character>
    {
        public IList<Character> GetAllByLogin(string login)
        {
            return Session.CreateCriteria<Character>().Add(Restrictions.Eq("AccountName", login)).List<Character>();
        }

        public Character GetById(int id)
        {
            return Session.CreateCriteria<Character>().Add(Restrictions.Eq("CharId", id)).UniqueResult<Character>();
        }

        public IList<Character> GetTopPvP()
        {
            return Session.CreateCriteria<Character>().AddOrder(Order.Desc("PvPKill")).SetMaxResults(15)
                .List<Character>();
        }

        public IList<Character> GetTopPk()
        {
            return Session.CreateCriteria<Character>().AddOrder(Order.Desc("PkKill")).SetMaxResults(15)
                .List<Character>();
        }

        public IList<Character> GetTopOnlineTime()
        {
            return Session.CreateCriteria<Character>().AddOrder(Order.Desc("OnlineTime")).SetMaxResults(15)
                .List<Character>();
        }
    }
}
