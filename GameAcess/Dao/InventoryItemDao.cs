﻿using System.Collections.Generic;
using GameAcess.Model;
using NHibernate.Criterion;

namespace GameAcess.Dao
{
    public class InventoryItemDao : GameDaoBase<InventoryItem>
    {
        public IList<InventoryItem> GetAllByCharId(int ownerId)
        {
            return Session.CreateCriteria<InventoryItem>().Add(Restrictions.Eq("OwnerId", ownerId)).List<InventoryItem>();
        }
    }
}
