﻿using System;
using System.IO;
using NHibernate;
using NHibernate.XFactories;

namespace GameAcess
{
    public class NHibernateSessionGame
    {
        private static ISessionFactory _factory;

        public static ISession Session
        {
            get
            {
                if (_factory != null) return _factory.OpenSession();

                NHibernate.Cfg.Configuration config = new NHibernate.Cfg.Configuration();
                _factory = config
                    .Configure(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "hibernate.cfg.xml"), "GameDB")
                    .BuildSessionFactory();

                return _factory.OpenSession();
            }
        }
    }
}