﻿using System.Collections.Generic;

namespace GameAcess.Interface
{
    internal interface IGameDaoBase<T>
    {
        IList<T> GetAll();
    }
}
