﻿namespace GameAcess.Model
{
    public class Character
    {
        public virtual int CharId { get; set; }
        public virtual string CharName { get; set; }
        public virtual string AccountName { get; set; }
        public virtual int Level { get; set; }
        public virtual int Race { get; set; }
        public virtual int Sex { get; set; }
        public virtual int PvPKill { get; set; }
        public virtual int PkKill { get; set; }
        public virtual int MaxHp { get; set; }
        public virtual int MaxCp { get; set; }
        public virtual int MaxMp { get; set; }
        public virtual long OnlineTime { get; set; }
        public virtual int Online { get; set; }
        public virtual int LocX { get; set; }
        public virtual int LocY { get; set; }
        public virtual int LocZ { get; set; }
    }
}