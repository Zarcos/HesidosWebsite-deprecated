﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GameAcess.Model
{
    public class InventoryItem
    {
        public virtual int ObjectId { get; set; }
        public virtual int OwnerId { get; set; }
        public virtual int ItemId { get; set; }
        public virtual long ItemCount { get; set; }
        public virtual int Enchant { get; set; }
    }
}
