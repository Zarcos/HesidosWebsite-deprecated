﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAcess.Dao;
using WebAcess.Model;

namespace HesidosWebsite.Controllers
{
    public class HtmlHelperController : Controller
    {
        [ChildActionOnly]
        public ActionResult Index()
        {
            return View();
        }
    }
}