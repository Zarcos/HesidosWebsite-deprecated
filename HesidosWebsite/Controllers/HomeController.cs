﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using GameAcess.Dao;
using GameAcess.Model;
using HesidosWebsite.Loader;
using HesidosWebsite.Models;
using Newtonsoft.Json;
using WebAcess.Dao;

namespace HesidosWebsite.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(int? page)
        {
            const string pageId = "307553353017043";
            const string accessToken = "EAAdFlgGpTjoBAIK1ErzfVYCoHNgbDsB5wfSP15Vxq081CThoVJkKBjAyDcZAyz39YLOgGrFepljyyLjZBR7vfGJnpOaTfA47o6e1FSfCu4XZByXWyLxwZAqpb0iTBiXOvXQ5ypYK8HefvpbbOZCwrzSrN3MKHZBrWiHzAuXN1IuAZDZD";
            FBPosts posts;
            var requestUrl = "https://graph.facebook.com/" + pageId + "/posts?access_token=" + accessToken;
            var request = (HttpWebRequest)WebRequest.Create(requestUrl);
            request.Method = "GET";
            request.Timeout = 100000;
            request.Accept = "application/json";
            request.ContentType = "application/json; charset=utf-8";
            WebResponse feedResponse = (HttpWebResponse)request.GetResponse();
           
            using (feedResponse)
            {
                using (var reader = new StreamReader(feedResponse.GetResponseStream()))
                {
                    posts = JsonConvert.DeserializeObject<FBPosts>(reader.ReadToEnd()); 
                }
            }

            const int itemOnPage = 5;
            var pg = page ?? 1;

            ViewBag.Pages = (int)Math.Ceiling(posts.data.Length /(double) itemOnPage);
            ViewBag.CurrentPage = pg;

            var from = (pg * itemOnPage) - itemOnPage;
            var lenght = itemOnPage > (posts.data.Length-from) ? posts.data.Length-from : itemOnPage;

            posts.data = posts.data.ToList<Messages>().GetRange(from, lenght).ToArray();

            if (Request.IsAjaxRequest())
            {
                return PartialView(posts);
            }
            return View(posts);
        }

        public ActionResult Changelog(int? page)
        {
            const int itemsOnPage = 5;
            var pg = page ?? 1;

            var changelogs = new ChangelogDao().GetChangelogsPaged(itemsOnPage, pg, out var totalTopics);

            ViewBag.Pages = (int) Math.Ceiling(totalTopics / (double)itemsOnPage);
            ViewBag.CurrentPage = pg;

            if (Request.IsAjaxRequest())
            {
                return PartialView(changelogs);
            }

            return View(changelogs);
        }

        public ActionResult Guide(int? page)
        {
            const int itemsOnPage = 10;
            var pg = page ?? 1;

            var guides = new GuideDao().GetGuidesPaged(itemsOnPage, pg, out var totalTopics);

            ViewBag.Pages = (int) Math.Ceiling(totalTopics / (double)itemsOnPage);
            ViewBag.CurrentPage = pg;

            if (Request.IsAjaxRequest())
            {
                return PartialView(guides);
            }

            return View(guides);
        }

        public ActionResult GuideDetail(int id, int? page)
        {
            ViewBag.CurrentPage = page ?? 1;

            var guide = new GuideDao().GetById(id);

            if (Request.IsAjaxRequest())
            {
                return PartialView(guide);
            }

            return View(guide);
        }

        public ActionResult Rules()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View();
        }

        public ActionResult Information()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View();
        }
    }
}