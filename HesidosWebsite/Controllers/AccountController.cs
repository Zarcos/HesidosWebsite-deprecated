﻿using System;
using System.IO;
using System.Net;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using System.Web.WebPages;
using HesidosWebsite.Class;
using HesidosWebsite.Models;
using LoginAcess.Dao;
using LoginAcess.Model;
using Newtonsoft.Json;
using WebAcess.Dao;
using WebAcess.Model;

namespace HesidosWebsite.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView(null);
            }
            return View();
        }

        public ActionResult Registration()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView(null);
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignIn(Account account, string password, string token)
        {
            if (token.IsEmpty()) return JavaScript("location.reload(true)");

            var request = (HttpWebRequest)WebRequest.Create("https://www.google.com/recaptcha/api/siteverify?secret=6LdDr3YUAAAAAI3jj6VWm3y1hYTERxhB4kyfN5Vg&response=" + token);
            request.ContentType = "application/json";
            request.Method = "POST";
            request.ContentLength = 0;
            Captcha captcha;
            var response = (HttpWebResponse)request.GetResponse();
            var responseStream = response.GetResponseStream();
            if (responseStream == null) return JavaScript("location.reload(true)");

            using (var sr = new StreamReader(responseStream))
            {
                captcha = JsonConvert.DeserializeObject<Captcha>(sr.ReadToEnd());
            }

            if (!captcha.success || !(captcha.score > 0.5)) return JavaScript("location.reload(true)");

            if (Membership.ValidateUser(account.Login, password.HashPassword()))
            {
                var websiteNick = new WebsiteUserDao().GetByLogin(account.Login);
                if (websiteNick == null)
                {
                    new WebsiteUserDao().Create(new WebsiteUser { Login = account.Login });
                }
                if (new WebsiteUserDao().GetByLogin(account.Login).WebsiteName.IsEmpty())
                {
                    account.ConfirmPassword = account.Password;
                    return PartialView("WebsiteName", account);
                }
                FormsAuthentication.SetAuthCookie(account.Login, false);
                return JavaScript("location.reload(true)");
            }

            TempData["error"] = "Username and password doesnt match";
            if (Request.IsAjaxRequest())
            {
                return PartialView("Index", account);
            }
            return View("Index", account);
        }

        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Clear();

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(Account account, string token)
        {
            if (token.IsEmpty()) return JavaScript("location.reload(true)");

            var request = (HttpWebRequest)WebRequest.Create("https://www.google.com/recaptcha/api/siteverify?secret=6LdDr3YUAAAAAI3jj6VWm3y1hYTERxhB4kyfN5Vg&response=" + token);
            request.ContentType = "application/json";
            request.Method = "POST";
            request.ContentLength = 0;
            Captcha captcha;
            var response = (HttpWebResponse)request.GetResponse();
            var responseStream = response.GetResponseStream();

            if (responseStream == null) return JavaScript("location.reload(true)");

            using (var sr = new StreamReader(responseStream))
            {
                captcha = JsonConvert.DeserializeObject<Captcha>(sr.ReadToEnd());
            }

            if (!captcha.success || !(captcha.score > 0.5)) return JavaScript("location.reload(true)");

            if (User.Identity.IsAuthenticated)
            {
                return JavaScript("location.reload(true)");
            }

            if (!ModelState.IsValid) return View("Registration", account);

            account.Password = account.Password.HashPassword();
            new WebsiteUserDao().Create(new WebsiteUser { Login = account.Login, WebsiteName = account.WebsiteUser.WebsiteName });
            new AccountDao().Create(account);
            FormsAuthentication.SetAuthCookie(account.Login, false);
            return JavaScript("location.reload(true)");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateWebsiteName(Account account)
        {
            if (new WebsiteUserDao().GetByNickname(account.WebsiteUser.WebsiteName) == null)
            {
                account.WebsiteUser.Login = account.Login;
                new WebsiteUserDao().Update(account.WebsiteUser);
                FormsAuthentication.SetAuthCookie(account.Login, false);
                return JavaScript("location.reload(true)");
            }
            ModelState.AddModelError("nickname","Nickname already exists");
            if (Request.IsAjaxRequest())
            {
                return PartialView("WebsiteName", account);
            }
            return View("WebsiteName", account);
        }

        [HttpPost]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult IsUsernameExist(string login)
        {
            return Json(new AccountDao().GetByLogin(login) == null);
        }

        [HttpPost]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult IsNicknameExist([Bind(Prefix = "WebsiteUser.WebsiteName")]string websiteName)
       {
            return Json(new WebsiteUserDao().GetByNickname(websiteName) == null);
        }
    }
}