﻿namespace HesidosWebsite.Class
{
    public class AccountAccessLevel
    {
        private int AccessLevel { get; set; }

        public AccountAccessLevel(int level)
        {
            AccessLevel = level;
        }

        public string[] GetRoles()
        {
            if (AccessLevel < 0)
            {
                return new [] {ServerRoles.Banned};
            }
            if (AccessLevel == 0)
            {
                return new []{ServerRoles.Player};
            }
            if (AccessLevel < 50 && AccessLevel >= 10)
            {
                return new[] {ServerRoles.Player, ServerRoles.Moderator};
            }
            if (AccessLevel < 100 && AccessLevel >= 50)
            {
                return new[] {ServerRoles.Player, ServerRoles.Moderator, ServerRoles.GameMaster};
            }

            return new[] {ServerRoles.Player, ServerRoles.Moderator, ServerRoles.GameMaster, ServerRoles.Admin};
        }
    }
}