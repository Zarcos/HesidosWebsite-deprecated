﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace HesidosWebsite.Class
{
    public static class PasswordTool
    {
        public static string HashPassword(this string str)
        {
            SHA1 shA1 = SHA1.Create();
            byte[] bytes = new ASCIIEncoding().GetBytes(str);
            str = Convert.ToBase64String(shA1.ComputeHash(bytes));
            return str;
        }
    }
}