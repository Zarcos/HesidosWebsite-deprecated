﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HesidosWebsite.Class
{
    public static class ServerRoles
    {
        public const string Banned = "banned";
        public const string Player = "player";
        public const string Moderator = "moderator";
        public const string GameMaster = "gamemaster";
        public const string Admin = "admin";
    }
}