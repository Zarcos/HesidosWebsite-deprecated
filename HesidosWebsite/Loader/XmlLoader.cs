﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Xml;
using GameAcess.Model;

namespace HesidosWebsite.Loader
{
    public static class XmlLoader
    {
        private static ConcurrentDictionary<int, Item> _itemList;

        public static ConcurrentDictionary<int, Item> GetItems()
        {
            return _itemList;
        }

        public static void Init()
        {
            var files = Directory.GetFileSystemEntries(HttpContext.Current.Server.MapPath("~/Content/xml"));
            Dictionary<int, Item> items = new Dictionary<int, Item>();
            foreach (var file in files)
            {
                var content = File.ReadAllText(file);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(content);
                XmlNodeList xnList = doc.SelectNodes("list/item"); //.ChildNodes;
                if (xnList != null)
                    foreach (XmlNode node in xnList)
                    {
                        if (node.Attributes != null)
                        {
                            int id = int.Parse(node.Attributes["id"].InnerText);
                            string name = node.Attributes["name"].InnerText;
                            string icon = string.Empty;
                            bool questItem = false;
                            foreach (XmlNode child in node.ChildNodes)
                            {
                                if (child?.Attributes?["name"]?.InnerText == "icon")
                                {
                                    icon = child.Attributes["val"].InnerText.Replace("icon.", string.Empty)
                                        .Replace("BranchSys.", string.Empty);
                                }

                                if (child?.Attributes?["name"]?.InnerText == "is_questitem")
                                {
                                    questItem = child.Attributes["val"].InnerText == "true";
                                }
                            }

                            var item = new Item
                            {
                                ItemId = id,
                                ItemName = name,
                                ItemImage = icon,
                                IsQuestItem = questItem
                            };
                            items.Add(id, item);
                        }
                    }
            }
            _itemList = new ConcurrentDictionary<int, Item>(items);
        }
    }
}