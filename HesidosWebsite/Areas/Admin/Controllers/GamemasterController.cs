﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HesidosWebsite.Class;

namespace HesidosWebsite.Areas.Admin.Controllers
{
    [Authorize(Roles = ServerRoles.GameMaster)]
    public class GamemasterController : Controller
    {
        // GET: Admin/Gamemaster
        public ActionResult LiveMap()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View();
        }

        public ActionResult Punishments()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View();
        }
    }
}