﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HesidosWebsite.Class;
using WebAcess.Dao;
using WebAcess.Model;

namespace HesidosWebsite.Areas.Admin.Controllers
{
    [Authorize(Roles = ServerRoles.Moderator)]
    public class ChatController : Controller
    {
        // GET: Admin/Chat
        public ActionResult Index()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View();
        }

        public ActionResult Chat()
        {
            IList<Message> messages = new MessageDao().GetLast10Messages();

            if (Request.IsAjaxRequest())
            {
                return PartialView(messages);
            }
            return View(messages);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Post(Message message)
        {
            message.Username = new WebsiteUserDao().GetByLogin(User.Identity.Name).WebsiteName;
            message.Date = DateTime.Now;

            if (ModelState.IsValid)
            {
                new MessageDao().Create(message);
            }
            return RedirectToAction("Index");
        }
    }
}