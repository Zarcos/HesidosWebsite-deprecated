﻿using System;
using System.Web.Mvc;
using HesidosWebsite.Class;
using WebAcess.Dao;
using WebAcess.Model;

namespace HesidosWebsite.Areas.Admin.Controllers
{
    [Authorize(Roles = ServerRoles.Moderator)]
    public class HomeController : Controller
    {
        public ActionResult Changelogs(int? page)
        {
            const int itemsOnPage = 10;
            var pg = page ?? 1;

            var changelogs = new ChangelogDao().GetChangelogsPaged(itemsOnPage, pg, out var totalTopics);

            ViewBag.Pages = (int) Math.Ceiling(totalTopics / (double)itemsOnPage);
            ViewBag.CurrentPage = pg;

            if (Request.IsAjaxRequest())
            {
                return PartialView(changelogs);
            }
            return View(changelogs);
        }

        public ActionResult ChangelogEdit(int id, int? page)
        {

            ViewBag.CurrentPage = page ?? 1;

            var changelog = new ChangelogDao().GetById(id);

            if (Request.IsAjaxRequest())
            {
                return PartialView(changelog);
            }
            return View(changelog);
        }

        public ActionResult ChangelogDelete(int id)
        {
            ChangelogDao changelogDao = new ChangelogDao();
            Changelog changelog = changelogDao.GetById(id);
            if (changelog != null)
            {
                changelogDao.Delete(changelog);
            }
            return RedirectToAction("Changelogs");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangelogSave(Changelog changelog, int? page)
        {
            ViewBag.CurrentPage = page ?? 1;

            if (ModelState.IsValid)
            {
                changelog.Date = DateTime.Now;
                new ChangelogDao().Update(changelog);

                return RedirectToAction("Changelogs");
            }

            return View("ChangelogEdit",changelog);
        }

        public ActionResult ChangelogCreate(Changelog changelog, int? page)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView(changelog);
            }
            return View(changelog);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangelogAdd(Changelog changelog)
        {
            if (ModelState.IsValid)
            {
                changelog.Date = DateTime.Now;
                new ChangelogDao().Create(changelog);

                return RedirectToAction("Changelogs");
            }

            return View("ChangelogCreate", changelog);
        }

        public ActionResult Guides(int? page)
        {
            const int itemsOnPage = 20;
            var pg = page ?? 1;

            var guides = new GuideDao().GetGuidesPaged(itemsOnPage, pg, out var totalTopics);

            ViewBag.Pages = (int) Math.Ceiling(totalTopics / (double)itemsOnPage);
            ViewBag.CurrentPage = pg;

            if (Request.IsAjaxRequest())
            {
                return PartialView(guides);
            }
            return View(guides);
        }

        public ActionResult GuideEdit(int id, int? page)
        {
            ViewBag.CurrentPage = page ?? 1;

            var guide = new GuideDao().GetById(id);

            if (Request.IsAjaxRequest())
            {
                return PartialView(guide);
            }
            return View(guide);
        }

        public ActionResult GuideDelete(int id)
        {
            GuideDao guideDao = new GuideDao();
            Guide guide = guideDao.GetById(id);
            if (guide != null)
            {
                guideDao.Delete(guide);
            }
            return RedirectToAction("Guides");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GuideSave(Guide guide, int? page)
        {
            ViewBag.CurrentPage = page ?? 1;

            if (ModelState.IsValid)
            {
                new GuideDao().Update(guide);

                return RedirectToAction("Guides");
            }

            return View("GuideEdit",guide);
        }

        public ActionResult GuideCreate(Guide guide, int? page)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView(guide);
            }
            return View(guide);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GuideAdd(Guide guide)
        {
            if (ModelState.IsValid)
            {
                new GuideDao().Create(guide);

                return RedirectToAction("Guides");
            }

            return View("GuideCreate", guide);
        }
    }
}