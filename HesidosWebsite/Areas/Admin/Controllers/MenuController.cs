﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HesidosWebsite.Areas.Admin.Controllers
{
    public class MenuController : Controller
    {
        [ChildActionOnly]
        public ActionResult Index()
        {
            return View();
        }
    }
}