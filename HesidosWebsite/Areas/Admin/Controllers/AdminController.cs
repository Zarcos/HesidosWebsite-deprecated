﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HesidosWebsite.Class;

namespace HesidosWebsite.Areas.Admin.Controllers
{
    [Authorize(Roles = ServerRoles.Admin)]
    public class AdminController : Controller
    {
        // GET: Admin/Admin
        public ActionResult ServerRestart()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View();
        }

        public ActionResult Annoucement()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View();
        }
    }
}