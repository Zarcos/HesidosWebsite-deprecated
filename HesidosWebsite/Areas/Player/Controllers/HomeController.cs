﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls.WebParts;
using GameAcess.Dao;
using GameAcess.Model;
using HesidosWebsite.Class;

namespace HesidosWebsite.Areas.Player.Controllers
{
    [Authorize(Roles = ServerRoles.Player)]
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            IList<Character> characters = new CharacterDao().GetAllByLogin(User.Identity.Name);
            if (Request.IsAjaxRequest())
            {
                return PartialView(characters);
            }
            return View(characters);
        }

        public ActionResult DonationMenu()
        {
            IList<Character> characters = new CharacterDao().GetAllByLogin(User.Identity.Name);
            if (Request.IsAjaxRequest())
            {
                return PartialView(characters);
            }
            return View(characters);
        }

        public ActionResult RepairCharMenu()
        {
            IList<Character> characters = new CharacterDao().GetAllByLogin(User.Identity.Name);
            if (Request.IsAjaxRequest())
            {
                return PartialView(characters);
            }
            return View(characters);
        }

        public ActionResult GetPlayerInventory(int charId)
        {
            IList<InventoryItem> items = new InventoryItemDao().GetAllByCharId(charId);

            return PartialView(items);
        }

        public ActionResult GetPlayerInfo(int charId)
        {
            Character character = new CharacterDao().GetById(charId);

            return PartialView(character);
        }

        public ActionResult GetCharRepairForm(int charId)
        {
            Character character = new CharacterDao().GetById(charId);

            return PartialView(character);
        }

        public ActionResult RepairCharAction(int charId)
        {
            CharacterDao characterDao = new CharacterDao();
            Character character = characterDao.GetById(charId);
            if (character.Online == 0)
            {
                character.LocX = 83016;
                character.LocY = 148632;
                character.LocZ = -3460;
                characterDao.Update(character);
                ViewBag.AlertMessageSuccess = "Your character was teleported to Town of Giran";
            }
            else
            {
                ViewBag.AlerMessageDanger = "Your character must be offline to use this function!";
            }
            IList<Character> characters = new CharacterDao().GetAllByLogin(User.Identity.Name);
            if (Request.IsAjaxRequest())
            {
                return PartialView("RepairCharMenu",characters);
            }
            return View("RepairCharMenu",characters);
        }

        public ActionResult GetDonationForm(int charId)
        {
            Character character = new CharacterDao().GetById(charId);

            return PartialView(character);
        }

        public ActionResult GetTopPvP()
        {
            IList<Character> characters = new CharacterDao().GetTopPvP();

            if (Request.IsAjaxRequest())
            {
                return PartialView("GetTopList", characters);
            }
            return View("GetTopList", characters);
        }

        public ActionResult GetTopPk()
        {
            IList<Character> characters = new CharacterDao().GetTopPk();

            if (Request.IsAjaxRequest())
            {
                return PartialView("GetTopList", characters);
            }
            return View("GetTopList", characters);
        }

        public ActionResult GetTopOnlineTime()
        {
            IList<Character> characters = new CharacterDao().GetTopOnlineTime();

            if (Request.IsAjaxRequest())
            {
                return PartialView("GetTopList", characters);
            }
            return View("GetTopList", characters);
        }
    }
}