﻿using System.Web.Mvc;

namespace HesidosWebsite.Areas.Player
{
    public class PlayerAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "Player";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Player_default",
                "Player/{controller}/{action}/{id}",
                new {action = "Index", id = UrlParameter.Optional },
                new []{"HesidosWebsite.Areas.Player.Controllers"}
            );
        }
    }
}