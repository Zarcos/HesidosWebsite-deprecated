﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Google.Protobuf.WellKnownTypes;

namespace HesidosWebsite.Models
{
    public class Captcha
    {
        public float score { get; set; }
        public bool success { get; set; }
    }
}