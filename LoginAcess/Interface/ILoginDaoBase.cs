﻿using System.Collections.Generic;

namespace LoginAcess.Interface
{
    internal interface ILoginDaoBase<T>
    {
        object Create(T entiry);
        void Update(T entity);
        T GetByLogin(string login);
    }
}
