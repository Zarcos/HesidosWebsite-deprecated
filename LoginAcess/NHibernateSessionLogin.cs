﻿using System;
using System.IO;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.XFactories;

namespace LoginAcess
{
    public class NHibernateSessionLogin
    {
        private static ISessionFactory _factory;

        public static ISession Session
        {
            get
            {
                if (_factory != null) return _factory.OpenSession();

                var cfg = new Configuration();

                _factory = cfg.Configure(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "hibernate.cfg.xml"),
                        "LoginDB")
                    .BuildSessionFactory();

                return _factory.OpenSession();
            }
        }
    }
}