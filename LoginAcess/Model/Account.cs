﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using System.Web.Security;
using WebAcess.Model;

namespace LoginAcess.Model
{
    public class Account : MembershipUser
    {
        [Microsoft.Build.Framework.Required]
        [RegularExpression("^[a-zA-Z0-9]+$", ErrorMessage = "Use letters and number only")]
        [MinLength(6)]
        [MaxLength(45)]
        [Display(Name = "Username")]
        [Remote("IsUsernameExist","Account", HttpMethod = "POST", ErrorMessage = "Username already exists")]
        public virtual string Login { get; set; }

        public virtual string AccountEmail { get; set; }

        [Microsoft.Build.Framework.Required]
        [DataType(DataType.Password)]
        [MaxLength(45)]
        public virtual string Password { get; set; }

        [NotMapped]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Passwords and Confirm Password don't match")]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }

        public virtual int AccessLevel { get; set; }

        [NotMapped]
        public WebsiteUser WebsiteUser { get; set; }

        public virtual int DonatePoints { get; set; }
    }
}
