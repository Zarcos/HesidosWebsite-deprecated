﻿using LoginAcess.Interface;
using NHibernate;
using NHibernate.Criterion;

namespace LoginAcess.Dao
{
    public class LoginDaoBase<T> : ILoginDaoBase<T> where T : class
    {
        protected ISession Session;

        protected LoginDaoBase()
        {
            Session = NHibernateSessionLogin.Session;
        }

        public object Create(T entiry)
        {
            object o;
            using (var transaction = Session.BeginTransaction())
            {
                o = Session.Save(entiry);
                transaction.Commit();
            }

            return o;
        }

        public T GetByLoginAndPassword(string login, string password)
        {
            return Session.CreateCriteria<T>().Add(Restrictions.Eq("Login", login))
                .Add(Restrictions.Eq("Password", password)).UniqueResult<T>();
        }

        public T GetByLogin(string login)
        {
            return Session.CreateCriteria<T>().Add(Restrictions.Eq("Login", login)).UniqueResult<T>();
        }

        public void Update(T entity)
        {
            using (var transaction = Session.BeginTransaction())
            {
                Session.Update(entity);
                transaction.Commit();
            }
        }
    }
}