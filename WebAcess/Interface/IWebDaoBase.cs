﻿using System.Collections.Generic;

namespace WebAcess.Interface
{
    internal interface IWebDaoBase<T>
    {
        IList<T> GetAll();
        object Create(T entiry);
        void Update(T entity);
        void Delete(T entity);
        T GetById(int id);
    }
}
