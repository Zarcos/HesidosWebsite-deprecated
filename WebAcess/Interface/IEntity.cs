﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace WebAcess.Interface
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
