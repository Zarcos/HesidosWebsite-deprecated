﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using WebAcess.Interface;

namespace WebAcess.Model
{
    public class Changelog : IEntity
    {
        public virtual int Id { get; set; }

        [Required]
        public virtual string Title { get; set; }

        [Required, AllowHtml]
        public virtual string Content { get; set; }

        public virtual DateTime Date { get; set; }
    }
}
