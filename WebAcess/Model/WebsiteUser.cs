﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebAcess.Model
{
    public class WebsiteUser
    {
        [MaxLength(45)]
        public virtual string Login { get; set; }
        [MaxLength(45)]
        [Required]
        [Display(Name = "Nickname")]
        [Remote("IsNicknameExist","Account", HttpMethod = "POST", ErrorMessage = "Nickname already exists")]
        public virtual string WebsiteName { get; set; }
    }
}
