﻿using System;
using System.ComponentModel.DataAnnotations;
using WebAcess.Interface;

namespace WebAcess.Model
{
    public class Message : IEntity
    {
        public virtual int Id { get; set; }
        public virtual string Username { get; set; }
        [Required]
        public virtual string Content { get; set; }
        public virtual DateTime Date { get; set; }
    }
}
