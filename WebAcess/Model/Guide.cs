﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebAcess.Interface;

namespace WebAcess.Model
{
    public class Guide : IEntity
    {
        public virtual int Id { get; set; }
        [Required]
        public virtual string Title { get; set; }
        [Required]
        public virtual string Description { get; set; }
        [Required]
        [AllowHtml]
        public virtual string Content { get; set; }
        [Required]
        public virtual string Creator { get; set; }
    }
}
