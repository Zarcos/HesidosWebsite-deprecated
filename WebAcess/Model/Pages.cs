﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebAcess.Interface;

namespace WebAcess.Model
{
    public class Pages : IEntity
    {
        public virtual int Id { get; set; }
        [Required]
        [AllowHtml]
        public virtual string Name { get; set; }
        [Required]
        [AllowHtml]
        public virtual string Content { get; set; }
        public virtual DateTime LastChange { get; set; }
    }
}
