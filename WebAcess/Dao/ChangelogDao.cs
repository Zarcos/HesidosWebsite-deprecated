﻿using System.Collections.Generic;
using NHibernate.Criterion;
using WebAcess.Model;

namespace WebAcess.Dao
{
    public class ChangelogDao : WebDaoBase<Changelog>
    {
        public IList<Changelog> GetChangelogsPaged(int count, int page, out int totalChangelogs)
        {
            totalChangelogs = Session.CreateCriteria<Changelog>()
                .SetProjection(Projections.RowCount())
                .UniqueResult<int>();

            return Session.CreateCriteria<Changelog>()
                .AddOrder(Order.Desc("Id"))
                .SetFirstResult((page - 1) * count)
                .SetMaxResults(count).List<Changelog>();
        }
    }
}
