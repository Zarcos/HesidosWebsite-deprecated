﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Criterion;
using WebAcess.Model;

namespace WebAcess.Dao
{
    public class MessageDao : WebDaoBase<Message>
    {
        public IList<Message> GetLast10Messages()
        {
            return Session.CreateCriteria<Message>().AddOrder(Order.Desc("Date")).SetMaxResults(10).List<Message>();
        }
    }
}
