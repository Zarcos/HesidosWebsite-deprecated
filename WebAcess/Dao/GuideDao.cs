﻿using System.Collections.Generic;
using NHibernate.Criterion;
using WebAcess.Model;

namespace WebAcess.Dao
{
    public class GuideDao : WebDaoBase<Guide>
    {
        public IList<Guide> GetGuidesPaged(int count, int page, out int totalChangelogs)
        {
            totalChangelogs = Session.CreateCriteria<Guide>()
                .SetProjection(Projections.RowCount())
                .UniqueResult<int>();

            return Session.CreateCriteria<Guide>()
                .SetFirstResult((page - 1) * count)
                .SetMaxResults(count).List<Guide>();
        }
    }
}
