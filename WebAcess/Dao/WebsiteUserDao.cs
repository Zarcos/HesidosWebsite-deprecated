﻿using NHibernate.Criterion;
using WebAcess.Model;

namespace WebAcess.Dao
{
    public class WebsiteUserDao : WebDaoBase<WebsiteUser>
    {
        public WebsiteUser GetByLogin(string login)
        {
            return Session.CreateCriteria<WebsiteUser>().Add(Restrictions.Eq("Login", login)).UniqueResult<WebsiteUser>();
        }

        public WebsiteUser GetByNickname(string websiteName)
        {
            return Session.CreateCriteria<WebsiteUser>().Add(Restrictions.Eq("WebsiteName", websiteName)).UniqueResult<WebsiteUser>();
        }
    }
}
