﻿using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using WebAcess.Interface;

namespace WebAcess.Dao
{
    public class WebDaoBase<T> : IWebDaoBase<T> where T:class
    {
        protected ISession Session;

        protected WebDaoBase()
        {
            Session = NHibernateSessionWeb.Session;
        }

        public IList<T> GetAll()
        {
            return Session.QueryOver<T>().List<T>();
        }

        public object Create(T entiry)
        {
            object o;
            using (var transaction = Session.BeginTransaction())
            {
                o = Session.Save(entiry);
                transaction.Commit();
            }

            return o;
        }

        public void Update(T entity)
        {
            using (var transaction = Session.BeginTransaction())
            {
                Session.Update(entity);
                transaction.Commit();
            }
        }

        public void Delete(T entity)
        {
            using (var transaction = Session.BeginTransaction())
            {
                Session.Delete(entity);
                transaction.Commit();
            }
        }

        public T GetById(int id)
        {
            return Session.CreateCriteria<T>().Add(Restrictions.Eq("id", id)).UniqueResult<T>();
        }
    }
}
